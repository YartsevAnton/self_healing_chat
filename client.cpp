#include "local.h"
#include "utils.h"
#include <cstring>
#include <strings.h>

using namespace std;

char message[BUF_SIZE]; //chat message buffer

/*---------------------main func to connect--------------------*/
int connect(char *argvv[])
{

	int sock, pid, pipe_fd[2], epfd, flag_connect=0;

	struct sockaddr_in addr;
	addr.sin_family = PF_INET;
	addr.sin_port = htons(SERVER_PORT);
	addr.sin_addr.s_addr = inet_addr(argvv[1]);

	static struct epoll_event ev, events[2]; //Socket(in|out) & Pipe(in)
	ev.events = EPOLLIN | EPOLLET;

	int continue_to_work = 1;

	CHK2(sock,socket(PF_INET, SOCK_STREAM, 0));

	//-------------------------------------evil_cycle_:]------------------------------------------
	while (flag_connect==0) //find server
	{
		addr.sin_port = htons(2345); //try to main
		if ( connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0 )
		{
			log_msg("[SYSTEM] could not connect to [MAIN server]", 4);
			addr.sin_port = htons(2346); //try to emergency
    		if ( connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0 )
				{
					log_msg("[SYSTEM] could not connect to [FIRST EMERGENCY server]", 4);
					addr.sin_port = htons(2347); //try to emergency
    				if ( connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0 )
					{
						log_msg("[SYSTEM] could not connect to [SECOND EMERGENCY server]", 4);
					}
					else 
					{
						log_msg("[SYSTEM] connected to [SECOND EMERGENCY server]", 5);
						flag_connect=1; //ok
					}
				}
			else 
				{
					log_msg("[SYSTEM] connected to [FIRST EMERGENCY server]", 5);
					flag_connect=1; //ok
				}
		}
		else 
		{
			log_msg("[SYSTEM] connected to [MAIN server]", 1);
			flag_connect=1; //ok
		}
	}
	//------------------------------------------------------------------------------------------------

	CHK(pipe(pipe_fd)); 
	CHK2(epfd,epoll_create(EPOLL_SIZE));
	ev.data.fd = sock; 
	CHK(epoll_ctl(epfd, EPOLL_CTL_ADD, sock, &ev));
	ev.data.fd = pipe_fd[0];
	CHK(epoll_ctl(epfd, EPOLL_CTL_ADD, pipe_fd[0], &ev));

	CHK2(pid,fork());

	switch(pid){
		case 0: // child process
			close(pipe_fd[0]);
			while(continue_to_work){
				bzero(&message, BUF_SIZE);
				fgets(message, BUF_SIZE, stdin);
				CHK(write(pipe_fd[1], message, strlen(message) - 1)); //send message
			}
			break;

		default: //parent process
			close(pipe_fd[1]);
			int epoll_events_count, res;

			while(continue_to_work) { //main cycle(epoll_wait)
				CHK2(epoll_events_count,epoll_wait(epfd, events, 2, EPOLL_RUN_TIMEOUT));
		
				for(int i = 0; i < epoll_events_count ; i++){
					bzero(&message, BUF_SIZE);

					if(events[i].data.fd == sock){ 	//new message from server
					
						CHK2(res,recv(sock, message, BUF_SIZE, 0));

						if(res == 0)
						{
							CHK(close(sock));
							continue_to_work = 0;
						}
						else printf("%s\n", message);
					}
					else
					{ 
						CHK2(res, read(events[i].data.fd, message, BUF_SIZE)); //event from child process(user's input message)
						if(res == 0) continue_to_work = 0;
						else //send message to server
						{
							CHK(send(sock, message, BUF_SIZE, 0));
						}
					}
				}
			}
	}
	if(pid)
	{
		log_msg("[SYSTEM] Connect lost. Reconnect", 4);
		close(pipe_fd[0]);
		close(sock);
	}
	else
	{
		close(pipe_fd[1]);
	}
	return 0;
}
/*---------------------------------------------------------------*/

int main(int argc, char *argv[])
{
	int main_flag = 0;
	while (main_flag==0)
	{
		main_flag = connect(argv);
	}
}