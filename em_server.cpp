#include "local.h"
#include "utils.h"
#include <cstring>
#include <strings.h>
using namespace std;

//list<int> clients_list;
//int listener;
//int epfd;
 
int main(int argc, char *argv[])
{
	struct sockaddr_in addr, their_addr;

	addr.sin_family = PF_INET;
	addr.sin_port = htons(EMERGENCY_SERVER_PORT);;
	addr.sin_addr.s_addr = inet_addr(argv[1]);

	socklen_t socklen;
	socklen = sizeof(struct sockaddr_in);

	static struct epoll_event ev, events[EPOLL_SIZE];

	ev.events = EPOLLIN | EPOLLET;
	char message[BUF_SIZE];
	
	clock_t tStart;
	int client, res, epoll_events_count;
	CHK2(listener, socket(PF_INET, SOCK_STREAM, 0));
	setnonblocking(listener);

	CHK(bind(listener, (struct sockaddr *)&addr, sizeof(addr)));
	//printf("Listener binded to: %s\n", argv[1]);

	CHK(listen(listener, 1));

	CHK2(epfd,epoll_create(EPOLL_SIZE));

	ev.data.fd = listener;

	CHK(epoll_ctl(epfd, EPOLL_CTL_ADD, listener, &ev));

	log_msg("[EMERGENCY server started]", 5);

	while(1)
	{
		CHK2(epoll_events_count,epoll_wait(epfd, events, EPOLL_SIZE, EPOLL_RUN_TIMEOUT));
		tStart = clock();

		for(int i = 0; i < epoll_events_count ; i++)
		{
			if(events[i].data.fd == listener)
			{
				CHK2(client,accept(listener, (struct sockaddr *) &their_addr, &socklen));
				setnonblocking(client);
				ev.data.fd = client;
				CHK(epoll_ctl(epfd, EPOLL_CTL_ADD, client, &ev));
				clients_list.push_back(client); // add new connection to list of clients
				bzero(message, BUF_SIZE);
			}
			else 
			{ //event for others(new incoming message from client)
				CHK2(res,handle_message_emergency_server(events[i].data.fd));
			}
		}
	}

	close(listener);
	close(epfd);

	return 0;
}