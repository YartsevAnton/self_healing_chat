#ifndef _SCHAT_LOCAL_H_
#define _SCHAT_LOCAL_H

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <list>
#include <time.h>

//For color text-----------------------------------
#define COLOR_RED "\033[31m" // red
#define COLOR_GRN "\033[32m" // green
#define COLOR_YEL "\033[33m" // yellow
#define COLOR_BLU "\033[34m" // blue
#define COLOR_MAG "\033[35m" // magneta
#define COLOR_CYN "\033[36m" // cyan
#define COLOR_WHT "\033[37m" // white
#define COLOR_OFF "\033[0m"  // reset
//--------------------------------------------------

//Default ports-------------------------------------
#define SERVER_PORT 2345
#define FIRST_EMERGENCY_SERVER_PORT 2346
#define SECOND_EMERGENCY_SERVER_PORT 2347
//--------------------------------------------------

//Command-------------------------------------------
#define CMD_SHUTDOWN "s.down"
#define CMD_LOG_SHOW "s.log_show"
#define CMD_LOG_CLEAR "s.log_clear"
#define CMD_CHECK_EM_STATUS "s.status"
//--------------------------------------------------

//Status_message------------------------------------
#define STR_status_em_fail "Emergy server down"
#define STR_status_em_ok "Emergy server ok"
//--------------------------------------------------

// Default buffer size
#define BUF_SIZE 1024

// Default timeout - http://linux.die.net/man/2/epoll_wait
#define EPOLL_RUN_TIMEOUT -1

// Count of connections that we are planning to handle (just hint to kernel)
#define EPOLL_SIZE 10000

// First welcome message from server
#define STR_WELCOME "You ID: #%d"

// Format of message population
#define STR_MESSAGE "Client #%d>> %s"



// Warning message if you alone in server
#define STR_NOONE_CONNECTED "You are alone"

// Macros - exit in any error (eval < 0) case
#define CHK(eval) if(eval < 0){perror("eval"); exit(-1);}

// Macros - same as above, but save the result(res) of expression(eval) 
#define CHK2(res, eval) if((res = eval) < 0){perror("eval"); exit(-1);}

// Preliminary declaration of functions
int setnonblocking(int sockfd);
void debug_epoll_event(epoll_event ev);
int handle_message(int new_fd);
int handling_message(int client, int server);
int print_incoming(int fd);

#endif
