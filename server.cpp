#include "local.h"
#include "utils.h"
#include <cstring>
#include <strings.h>
#include <cstring>

using namespace std; 

int main(int argc, char *argv[])
{
	//config for client----------------------------------------------
	struct sockaddr_in addr, their_addr;
	addr.sin_family = PF_INET; 
	addr.sin_port = htons(SERVER_PORT); //from local.h
	addr.sin_addr.s_addr = inet_addr(argv[1]); //from command line
	//---------------------------------------------------------------

	socklen_t socklen; socklen = sizeof(struct sockaddr_in);

	static struct epoll_event ev, events[EPOLL_SIZE];

	ev.events = EPOLLIN | EPOLLET;

	char message[BUF_SIZE]; //message buf

	clock_t tStart; //for timeout

	int client, 
		res, 
		epoll_events_count;

	CHK2(listener, socket(PF_INET, SOCK_STREAM, 0));  //for clients

	setnonblocking(listener); //make nonblock

	bind(listener, (struct sockaddr *)&addr, sizeof(addr)); //for clients

	listen(listener, 1);	//start listen to clients
	CHK2(epfd,epoll_create(EPOLL_SIZE));

	ev.data.fd = listener;

	epoll_ctl(epfd, EPOLL_CTL_ADD, listener, &ev);

	log_msg("[MAIN server started]", 1);
 
	while(1)
	{
		CHK2(epoll_events_count,epoll_wait(epfd, events, EPOLL_SIZE, EPOLL_RUN_TIMEOUT));
		tStart = clock();

		for(int i = 0; i < epoll_events_count ; i++)
		{
			if(events[i].data.fd == listener)
			{
				CHK2(client,accept(listener, (struct sockaddr *) &their_addr, &socklen));
				setnonblocking(client);
				ev.data.fd = client;
				CHK(epoll_ctl(epfd, EPOLL_CTL_ADD, client, &ev));
				printf(">new client\n");
				clients_list.push_back(client); // add new connection to list of clients
				bzero(message, BUF_SIZE);
			}
			else 
			{
				CHK2(res,handling_message(events[i].data.fd, 0));
			}
		}
	}
	close(listener);
	close(epfd); 

	return 0;
}