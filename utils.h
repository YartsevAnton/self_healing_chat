#include "local.h"
#include <cstdlib>
#include "local.h"
#include <cstring>
#include <strings.h>
#include <stdio.h>

using namespace std;

list<int> clients_list;
int listener,   //for clients
    listener_2; 
int epfd;

FILE *f;

// Setup nonblocking socket-------------------------------------------
int setnonblocking(int sockfd)
{
	CHK(fcntl(sockfd, F_SETFL, fcntl(sockfd, F_GETFD, 0)|O_NONBLOCK));
	return 0;
}
//---------------------------------------------------------------------

//for color log message--------------
void log_msg(char *msg, int color)
{
    switch(color)
    {
        case 1: // green
            printf("%s", COLOR_GRN);
            break;

        case 2: // yellow
            printf("%s", COLOR_YEL);
            break;

        case 3: // blue
            printf("%s", COLOR_BLU);
            break;

        case 4: //red
            printf("%s", COLOR_RED);
            break;

        case 5: // cyan
            printf("%s", COLOR_CYN);
            break;

        default: //white
            printf("%s", COLOR_WHT);
            break;
    }

    printf("%s", msg);
    printf("%s\n",COLOR_OFF);
}
//------------------------------------

//-------------------------------------------------for_main_server------------------------------------------------
int handling_message(int client, int server)
{
    //server table:
    //0 - main
    //1 - first_emergency
    //2 - second_emergency
 
    char buf[BUF_SIZE], message[BUF_SIZE];
    bzero(buf, BUF_SIZE);
    bzero(message, BUF_SIZE);
    int len, c;
    char test[BUF_SIZE];

    CHK2(len,recv(client, buf, BUF_SIZE, 0));

    if(len == 0)
    {
        CHK(close(client));
        clients_list.remove(client);;
    }
    else
    { 
        //message log------------------------------------------------------
        sprintf(message, STR_MESSAGE, client, buf);
        printf("[LOG]: ");
        log_msg(message, 1);
        f = fopen("log.txt", "a");
        fprintf(f, "%s\n", message);
        fclose(f);
        //-----------------------------------------------------------------

        //command shutdown-------------------------------------------------
        if (strcmp (buf, CMD_SHUTDOWN)==0)
        {
            close(listener);
            close(epfd);
        }
        //-----------------------------------------------------------------

        //show_log---------------------------------------------------------
        if (strcmp (buf, CMD_LOG_SHOW)==0)
        {
            f = fopen("log.txt", "r");


            while( (c = getc(f)) != EOF)
            putchar(c);
            printf("%c", c);


            fclose(f);
        }
        //------------------------------------------------------------------

         //clear_log---------------------------------------------------------
        if (strcmp (buf, CMD_LOG_CLEAR)==0)
        {
            f = fopen("log.txt", "w+");
            fclose(f);
        }
        //------------------------------------------------------------------

        //check_em_status----------------------------------------------------
        if (strcmp (buf, CMD_CHECK_EM_STATUS)==0)
        {
            struct sockaddr_in addr, their_addr;
            addr.sin_family = PF_INET; 
            addr.sin_port = htons(2346); //try to em

            int sock;
            CHK2(sock,socket(PF_INET, SOCK_STREAM, 0));
        
            if ( connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0 )
            {
                sprintf(message, STR_status_em_fail ); //emergy server is down
                send(client, message, BUF_SIZE, 0);
            }
            else
            {
                sprintf(message, STR_status_em_ok); //emergy server ok
                send(client, message, BUF_SIZE, 0);
            }
        }
        //--------------------------------------------------------------------
         

        //send message ALONE to user-----------------------------------------
        if(clients_list.size() == 1) 
        { 
        CHK(send(client, STR_NOONE_CONNECTED, strlen(STR_NOONE_CONNECTED), 0));
                return len;
        }
        //--------------------------------------------------------------------




        sprintf(message, STR_MESSAGE, client, buf);

            struct sockaddr_in addr, their_addr;
            addr.sin_family = PF_INET; 
            addr.sin_port = htons(2346); //try to em

            int sock;
            CHK2(sock,socket(PF_INET, SOCK_STREAM, 0));
            connect(sock, (struct sockaddr *)&addr, sizeof(addr));
            send(sock, message, BUF_SIZE, 0);

       
        //send message to all user
        list<int>::iterator it;
        for(it = clients_list.begin(); it != clients_list.end(); it++)
        {
           if(*it != client){ 
            CHK(send(*it, message, BUF_SIZE, 0));
           }
        }


    }
    return len;
}
//----------------------------------------------------------------------------------------------------------------